package it.damianocantali.rest.crm.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import it.damianocantali.rest.crm.models.Provincia;

public interface ProvinceRepository extends JpaRepository<Provincia, Long> {
	// TROVA LE PROVINCIE TRAMITE IL LORO ACRONIMO
	public Provincia findByAcronym(String acronym);

}
