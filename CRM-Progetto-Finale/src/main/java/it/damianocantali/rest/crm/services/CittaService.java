package it.damianocantali.rest.crm.services;


import it.damianocantali.rest.crm.models.Citta;
import it.damianocantali.rest.crm.models.Provincia;

public interface CittaService {
		Citta add(Citta citta);
		Citta findByName(String name);
		Citta findByNameAndProvince(String name,Provincia province);
}
