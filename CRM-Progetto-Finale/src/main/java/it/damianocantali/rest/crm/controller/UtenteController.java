package it.damianocantali.rest.crm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.data.domain.Sort;
import it.damianocantali.rest.crm.models.Utente;
import it.damianocantali.rest.crm.models.pojo.UtentePojo;
import it.damianocantali.rest.crm.repositories.UtenteRepository;
import it.damianocantali.rest.crm.serviceImpl.UtenteImp;

@RestController
@RequestMapping("/utente")
public class UtenteController {

	@Autowired
	UtenteImp imp;
	@Autowired
	UtenteRepository repository;
	
	/**
	 * @return salvataggio dell'utente e associazione del ruolo tramite id (enum RoleType)
	 */
	@PostMapping("/salvautente/{id}")
	@PreAuthorize("hasRole('ROLE_ADMIN') ")
	public Utente salvaUtente(@PathVariable long id, @RequestBody UtentePojo utente) {
		try {
			return imp.add(utente, id);
		} catch (Exception e) {
			throw new RuntimeException();

		}

	}
	/**
	 * @return tutti gli utenti paginati e ordinati
	 */
	@GetMapping(value = "/getallutentebysort")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public Page<Utente> getAllUtentePageSort(@RequestParam(defaultValue = "0") Integer page,
			@RequestParam(defaultValue = "8") Integer size, @RequestParam(defaultValue = "ASC") String dir,
			@RequestParam(defaultValue = "id") String sort) {
		Pageable pageable = PageRequest.of(page, size, Sort.Direction.valueOf(dir), sort);
		return imp.getAllUser(pageable);

	}
	/**
	 * @return eliminazione dell'utente
	 */
	@PostMapping("/eliminautente/{utente}")
	@PreAuthorize("hasRole('ROLE_ADMIN') ")
	public Utente delete(@PathVariable long utente) {
		return imp.delete(utente);
	}

}
