package it.damianocantali.rest.crm.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import it.damianocantali.rest.crm.models.Indirizzo;
import it.damianocantali.rest.crm.serviceImpl.IndirizzoServiceImp;

@RestController
@RequestMapping("/indirizzo")
public class IndirizzoController {
	@Autowired
	IndirizzoServiceImp indirizzoServiceImp;
	
	/**
	 * @return salvataggo di una indirizzo
	 */
	@PostMapping("/salvaindirizzo/{nomecomune}/{siglaprov}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String saveContatto(@PathVariable String nomecomune, @PathVariable String siglaprov,
			@RequestBody Indirizzo indirizzo) {
		indirizzoServiceImp.save(indirizzo, nomecomune, siglaprov);
		return "indirizzo salvato con successo";
	}
	/**
	 * @return aggiornamento di una fattura
	 */
	@PostMapping("/aggiornaindirizzzo/{nomecomune}/{siglaprov}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String updateContatto(@PathVariable String nomecomune, @PathVariable String siglaprov,
			@RequestBody Indirizzo indirizzo) {
		indirizzoServiceImp.update(indirizzo, nomecomune, siglaprov);
		return "Indirizzo aggiornato con successo";
	}

}
