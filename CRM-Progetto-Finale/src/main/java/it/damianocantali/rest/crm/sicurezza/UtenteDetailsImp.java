package it.damianocantali.rest.crm.sicurezza;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import com.fasterxml.jackson.annotation.JsonIgnore;
import it.damianocantali.rest.crm.models.Utente;
import lombok.Data;

@Data
public class UtenteDetailsImp implements UserDetails {

	private static final long serialVersionUID = -7130684040991684077L;

	private int id;
	private String userName;
	@JsonIgnore
	private String password;

	private boolean accountNonLocked = true;
	private boolean accountNonExpired = false;
	private boolean credentialsNonExpired = true;
	private boolean enabled = true;
	private Date expirationTime;

	// GrantedAuthority, rappresenta un'autorizzazione concessa (letture, scrittura,
	// ecc)
	private Collection<? extends GrantedAuthority> authorities;

	// Costruttore
	public UtenteDetailsImp(int id, String userName, String password,
			Collection<? extends GrantedAuthority> authorities) {
		super();
		this.id = id;
		this.userName = userName;
		this.password = password;
		this.accountNonLocked = enabled;
		this.accountNonExpired = enabled;
		this.credentialsNonExpired = enabled;
		this.authorities = authorities;
	}

	public static UtenteDetailsImp build(Utente utente) {
		// SimpleGrantedAuthority, implementazione concreta di base di una
		// GrantedAuthority
		List<GrantedAuthority> authorities = utente.getRole().stream()
				.map(role -> new SimpleGrantedAuthority(role.getRoleType().name())).collect(Collectors.toList());
		// Restituisce i dati dello user e la lista delle sue autorizzazioni
		return new UtenteDetailsImp((int) utente.getId(), utente.getUserName(), utente.getPassword(), authorities);
	}

	@Override
	public String getUsername() {

		return this.userName;
	}
}
