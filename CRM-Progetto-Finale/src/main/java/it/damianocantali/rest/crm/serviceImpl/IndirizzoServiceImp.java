package it.damianocantali.rest.crm.serviceImpl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.damianocantali.rest.crm.models.Citta;
import it.damianocantali.rest.crm.models.Fattura;
import it.damianocantali.rest.crm.models.Indirizzo;
import it.damianocantali.rest.crm.models.Provincia;
import it.damianocantali.rest.crm.repositories.CittaRepository;
import it.damianocantali.rest.crm.repositories.IndirizzoRepository;
import it.damianocantali.rest.crm.repositories.ProvinceRepository;
import it.damianocantali.rest.crm.services.IndirizzoService;
@Service
public class IndirizzoServiceImp implements IndirizzoService{
	
	@Autowired
	IndirizzoRepository indirizzoRepository;
	@Autowired
	CittaRepository cittaRepository;
	@Autowired
	ProvinceRepository provinceRepository ;

	@Override
	public Optional<Indirizzo> getById(long id) {
		try {
			return indirizzoRepository.findById(id);
		} catch (Exception e) {
			throw new RuntimeException(e);
			
		}     
	}

	@Override
	public Indirizzo save(Indirizzo indirizzo,String nomecomune, String siglaprov) {
		try {
			Provincia provincia = provinceRepository.findByAcronym(siglaprov);
		Citta c=	cittaRepository.findByNameAndProvince(nomecomune,provincia);
			indirizzo.setComune(c);
			return indirizzoRepository.save(indirizzo);
		} catch (Exception e) {
			throw new RuntimeException(e);
			
		}
	}

	@Override
	public Indirizzo update(Indirizzo indirizzo,String nomecomune, String siglaprov) {
		try {
			Provincia provincia = provinceRepository.findByAcronym(siglaprov);
			Citta c=	cittaRepository.findByNameAndProvince(nomecomune,provincia);
			indirizzo.setComune(c);
			Indirizzo f= indirizzoRepository.findById(indirizzo.getId()).get();
			f.setCap(indirizzo.getCap());
			f.setCivico(indirizzo.getCivico());
			f.setVia(indirizzo.getVia());
			f.setLocalita(indirizzo.getLocalita());
			return indirizzoRepository.save(f );
		} catch (Exception e) {
			throw new RuntimeException(e);
			
		}
	}

	
}
