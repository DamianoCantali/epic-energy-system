package it.damianocantali.rest.crm.models.pojo;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DatePojo {
		private Date data1;
		private Date data2;
	}

