package it.damianocantali.rest.crm.models.pojo;

import java.math.BigDecimal;
import java.util.Date;
import it.damianocantali.rest.crm.models.StatoFattura;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FatturaPojo {
	long id;
	private Date data;
	private BigDecimal importo;
	private String statoFattura;
	private int numero;
}
