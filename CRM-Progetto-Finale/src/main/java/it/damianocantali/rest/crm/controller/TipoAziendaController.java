package it.damianocantali.rest.crm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import it.damianocantali.rest.crm.models.TipoAzienda;
import it.damianocantali.rest.crm.serviceImpl.TipoAziendaImp;

@RestController
@RequestMapping("/tipoazienda")
public class TipoAziendaController {

	@Autowired
	TipoAziendaImp aziendaService;
	
	/**
	 * @return salvataggo di un TipoAzienda
	 */
	@PostMapping("/salvatipoazienda")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String saveTipoAzienda(@RequestBody TipoAzienda tipoAzienda) {
		aziendaService.save(tipoAzienda);
		return "Tipo dell'azienda salvato con successo";

	}
	/**
	 * @return eliminazione di un TipoAzienda
	 */
	@PostMapping("/eliminatipoazienda/{id}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String eliminaContatto(@PathVariable long id) {
		TipoAzienda a = aziendaService.getById(id).get();
		return aziendaService.delete(a);

	}
}