package it.damianocantali.rest.crm.models;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name="role")
@Getter
@Setter
@NoArgsConstructor
public class Role extends BaseEntity {
    
    
    
   
   
    @Enumerated(EnumType.STRING)
    private RoleType roleType;
    
    
    
    public Role(RoleType roleType) {
        this.roleType = roleType;
    }
    
    @Override
    public String toString() {
        return String.format("Role:  roletype%s ",  roleType);
        }
    }