package it.damianocantali.rest.crm.services;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import it.damianocantali.rest.crm.models.Fattura;
import it.damianocantali.rest.crm.models.StatoFattura;

@Repository
public interface FatturaService {
    public List<Fattura> getByAzienda(long id );
    public Page<Fattura> getByStatoFattura(StatoFattura statoFattura ,Pageable pageable);
    public Page<Fattura> getByData(Date data,Date data2,Pageable pageable);
    public Page<Fattura> getByDataYear(int anno ,Pageable pageable);
    public Page<Fattura> getByImportoBetween(BigDecimal primoImporto, BigDecimal secondoImporto,Pageable pageable);   
    public Optional<Fattura> getById(long id);
    
    public Fattura save(Fattura fattura);
    public Fattura update(Fattura fattura);
    public String delete(long id);
}
