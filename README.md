# Epic Energy System





Occorre realizzare il backend di un sistema CRM per un'azienda fornitrice di energia, denominata "EPIC ENERGY SERVICES", che vuole gestire i contatti con i propri clienti business.
Il sistema, basato su Web Service REST Spring Boot e database PostgreSQL, deve permettere di gestire un elenco dei clienti, che sono caratterizzati dai seguenti dati:



--MODELLI--

AZIENDA


ragioneSociale -Stringa
partitaIva - Long
email -Stringa
dataInserimento -Date
dataUltimoContatto -Date
fatturatoAnnuale -Long
pec	-String
telefono - Long
Contatto - Contatto

CONTATTO

emailContatto -String
nomeContatto - String
cognomeContatto -String
telefonoContatto -String
Tipo contatto - TipoContatto

Ogni cliente può avere fino a due indirizzi, uno per la sede legale ed uno per la sede operativa.
Un indirizzo è composto da

INDIRIZZO


via -String
civico -int
località -String
cap	-Int
comune -Comune

I comuni sono gestiti attraverso un'anagrafica centralizza e sono caratterizzati da un nome e da un riferimento ad una provincia, anch'essa gestita in anagrafica centralizzata e caratterizzata da un nome ed una sigla.
I clienti possono essere di vario tipo:

TIPO CONTATTO

Tipo - String

PA
SAS
SPA
SRL

Associato ad ogni cliente c'è un insieme di fatture. Le fatture sono caratterizzate dai seguenti dati:

FATTURA

anno : Integer
data : Date
importo : BigDecimal
numero : Integer




-- SERVIZI --


Ogni fattura ha uno stato. Gli stati fattura possono essere dinamici, in quanto in base all'evoluzione del business possono essere inseriti nel sistema nuovi stati.
Il back-end deve fornire al front-end tutte le funzioni necessarie a gestire in modo completo (Aggiungere, modificare ed eleiminare)i suddetti elementi.
Deve essere possibile ordinare i clienti per:

Nome
Fatturato annuale
Data di inserimento
Data di ultimo contatto
Provincia della sede legale.

Deve essere possibile filtrare la lista clienti per:

Fatturato annuale
Data di inserimento
Data di ultimo contatto
Parte del nome

Deve essere possibile filtrare le fatture per

Cliente
Stato
Data
Anno
Range di importi

Per gestire in modo efficiente un numero cospicuo di elementi, occorre utilizzare la paginazione.
Prevedere inoltre un sistema di autenticazione e autorizzazione basato su token JWT che permetta a diversi utenti di accedere alle funzioni del backend e di registrarsi al sistema. Un utente è caratterizzato dalle seguenti proprietà:

username
email
password
nome
cognome

Gli utenti possono essere di tipo USER, abilitato alle sole operazioni di lettura, oppure ADMIN, abilitato a tutte le operazioni. Un utente può avere più ruoli.

Importazione Comuni e Province
Viene fornito un elenco dei comuni in formato CSV (comuni-italiani.csv), che deve essere importato nel sistema per mezzo di una appositoa procedura Java da eseguire manualmente per popolare il db. Viene fornito inoltre un secondo file (province-italiane.csv) contenente la corrispondenza tra nome provincia e sigla ed anch'esso deve essere importato ed integrato con le informazioni relative ai comuni.

Contestualmente alla realizzazione del sistema occorre inoltre:

Realizzare una collection Postman contenente tutte le chiamate alle API del servizio, comprese quelle di autenticazione
Implementare i principali test con JUnit

****** ESERCIZI EXTRA ******
Si riportano due tracce di esercizi extra da proporre agli studenti nel caso in cui essi riescano a completare il progetto principale in anticipo rispetto alla scadenza prevista. Da proporre a discrezione del docente.


realizzare un piccolo frontend per il nostro applicativo/api di backend, sfruttando le tecnologie viste nella settimana 8, giornata 4, ovvero Thymeleaf. Realizzare un piccolo portalino con delle pagine che permettano l'accesso alle funzioni CRUD e di ricerca sulle entità Cliente, Fattura, Utente, Comune, Provincia. Per confezionare e adeguare rapidamente l'estetica del portale, sfruttare una libreria come Bootstrap, da inserire nei contenuti static della nostra app SpringBoot.


realizzare un piccolo servizio separato, secondo l'architettura MicroServices, vista nella settimana 10, giornata 3, che interroghi parte delle API presenti nel resto dell'applicazione, per la realizzazione di statistiche sui clienti, basate sulla categoria di fatturato e distribuzione sul territorio nazionale (provincia/regione). A sua volta, questo servizio secondario dovrà essere richiamato dal client costruito al punto 1. per la realizzazione di una "dashboard" di visualizzazione e distribuzione clienti.
