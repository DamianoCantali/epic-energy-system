package it.damianocantali.rest.crm.services;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import it.damianocantali.rest.crm.models.StatoFattura;

public interface StatoService {
	public Optional <StatoFattura>getById(long id);
	public StatoFattura save(StatoFattura statoFattura);
	public StatoFattura update(StatoFattura statoFattura);
	public Page<StatoFattura>findByStato(String stato,Pageable pageable);
	StatoFattura findBySingleStato(String stato);
}
