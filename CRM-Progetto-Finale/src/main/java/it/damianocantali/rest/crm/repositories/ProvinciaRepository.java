package it.damianocantali.rest.crm.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.damianocantali.rest.crm.models.Provincia;

@Repository
public interface ProvinciaRepository extends JpaRepository<Provincia, Integer> {
	// recupera una provincia tramite la sigla
	Optional<Provincia> findByAcronym(String acronym);
}
