package it.damianocantali.rest.crm.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.damianocantali.rest.crm.models.Role;
import it.damianocantali.rest.crm.models.RoleType;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
	// TROVA IL RUOLO TRAMITE ROLETYPE
	Role findByRoleType(RoleType r);

}
