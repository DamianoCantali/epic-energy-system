package it.damianocantali.rest.crm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.damianocantali.rest.crm.models.StatoFattura;
import it.damianocantali.rest.crm.serviceImpl.StatoServiceImp;

@RestController
@RequestMapping("/statoFattura")
public class StatoFatturaController {

	@Autowired
	StatoServiceImp serviceImp;
	/**
	 * @return salvataggo di uno Statofattura
	 */
	@PostMapping("/salvastatofattura")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String saveStatoFattura(@RequestBody StatoFattura statoFattura) {
		serviceImp.save(statoFattura);
		return "StatoFattura salvato con successo";
	}
	/**
	 * @return aggiornamento di uno Statofattura
	 */
	@PostMapping("/aggiornastatofattura")
	public String updateStatoFattura(@RequestBody StatoFattura statoFattura) {
		serviceImp.update(statoFattura);
		return "StatoFattura salvato con successo";
	}

}
