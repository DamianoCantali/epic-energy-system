package it.damianocantali.rest.crm.repositories;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import it.damianocantali.rest.crm.models.Azienda;
import it.damianocantali.rest.crm.models.Fattura;
import it.damianocantali.rest.crm.models.StatoFattura;

public interface FatturaRepository extends JpaRepository<Fattura, Long>{
	// TROVA TUTTE LE FATURE PER AZIENDA
	public List<Fattura> findAllByAzienda(Azienda azienda);
	// TROVA LA FATURA PER AZIENDA
	public List<Fattura> findByAzienda(Azienda azienda );
	// TROVA LA FATURA PER ID
	public Optional<Fattura>findById(Fattura fattura);
	// TROVA LA FATURA PER STATOFATTURA
	public Page<Fattura> findByStatoFattura(StatoFattura statoFattura ,Pageable pageable);
	// TROVA LA FATURA IN UN INTERVALLO DI TEMPO
	public Page<Fattura> findByDataBetween(Date data,Date data2,Pageable pageable);
	// TROVA TUTTE LE FATURE PER DATA DI EMISSIONE
	@Query("select f from Fattura as f where YEAR(f.data) = :anno")
	public Page<Fattura> findByDataYear(int anno ,Pageable pageable);
	// TROVA TUTTE LE FATURE IN UN INTERVALLO DI IMPORTI FATTURA
	public Page<Fattura> findByImportoBetween(BigDecimal primoImporto, BigDecimal secondoImporto,Pageable pageable);
	// TROVA TUTTE LE FATURE DI UN'AZIENDA IN UN DATO ANNO PER IL SUO ID
	@Query("SELECT SUM(f.importo) FROM Fattura f WHERE YEAR (f.data) = :anno AND f.azienda.id= :id")
   public BigDecimal getFatturatoAnnualeFromAzienda(int anno, long id);
	//public Fattura save(Fattura fattura);
}  


			