package it.damianocantali.rest.crm;

import java.io.FileInputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import be06.epicode.cities.CitiesLoader;
import be06.epicode.cities.models.City;
import it.damianocantali.rest.crm.models.Azienda;
import it.damianocantali.rest.crm.models.Citta;
import it.damianocantali.rest.crm.models.Contatto;
import it.damianocantali.rest.crm.models.Indirizzo;
import it.damianocantali.rest.crm.models.Provincia;
import it.damianocantali.rest.crm.models.Role;
import it.damianocantali.rest.crm.models.TipoAzienda;
import it.damianocantali.rest.crm.models.pojo.UtentePojo;
import it.damianocantali.rest.crm.repositories.CittaRepository;
import it.damianocantali.rest.crm.repositories.ProvinciaRepository;
import it.damianocantali.rest.crm.serviceImpl.AziendaServiceImp;
import it.damianocantali.rest.crm.serviceImpl.CittaServiceImpl;
import it.damianocantali.rest.crm.serviceImpl.UtenteImp;

@Component
public class CrmRunner implements CommandLineRunner{
	@Autowired
	CittaRepository cittaRepository;
	@Autowired
CittaServiceImpl  cittaServiceImpl;
	@Autowired 
	AziendaServiceImp aziendaServiceImp;
	@Autowired
	UtenteImp imp;

	@Override
			
	/**
	 *  il codice commentato permette di caricare nel Database tramite un file CSV, 
	 *  le citta e le provincie con le relative informazioni.
	 */
	public void run(String... args) throws Exception {
//		String fileName = "C:\\Users\\damiano\\git\\EnergySystem\\CRM-Progetto-Finale\\comuni.csv";
//		Set<City> cities= CitiesLoader.load(new FileInputStream(fileName));
//		 cities.stream().map(c -> Citta.builder() 
//	                .capital(c.isCapital())
//	                .name(c.getName())
//	                .province(Provincia.builder()
//	                        .name(c.getProvince().getName())
//	                        .acronym(c.getProvince().getAcronym())
//	                        .build()
//	                        )
//	                .build())
//	                .forEach(c -> cittaServiceImpl.add(c));
	//Contatto c = new Contatto("contatto@gmail.com","fabio","zucchetti",56855252);
	
	//	Azienda a = new Azienda("Zucchetti","consulenza"	,new BigDecimal(10000000),"ZUCCHETTI@GMAIL.COM",10000000,10/2/2022,"zucchetti@pec.it","095558877",new Contatto("contatto@gmail.com","Fabio","Capace",32856585),new TipoAzienda("SPA"),new Indirizzo("via zucchetti","55","zona industriale",20122,cittaRepository.findByNome("Milano"),new Indirizzo("via zucchetti Legale","66","zona centro",02200,cittaRepository.findByNome("Roma"))));
			
			
//		Azienda b = new Azienda().builder().nome("Zucchetti")
//				.ragioneSociale("consulenza")
//				.partitaIva(1234569)
//				.email("ZUCCHETTI@GMAIL.COM")
//				.fatturatoAnnuale(new BigDecimal(10000000))
//				.dataUltimoContatto(new Date(12/1/2022))
//		.pec("ZUCCHETTI@GMAIL.COM")
//					.telefono("32565895")
//				.contatto(new Contatto("contatto@gmail.com","Fabio","zucchetto",1461632169))
//					.tipoAzienda(new TipoAzienda("SPA"))
//				.indirizzoOperativo(new Indirizzo("via zucchetti","55","zona industriale",022255,cittaRepository.findByName("Milano")))
//				.indirizzoLegale(new Indirizzo("via zucchetti legle","66","zona centro",256155,cittaRepository.findByName("Roma"))).build();
////			
//                 aziendaServiceImp.save(b);
////					
//			List<String>list = new ArrayList<>();
//			list.add("ROLE_ADMIN");
			
		//imp.criptAll();
	//	UtentePojo ut = new UtentePojo( "giacomo", "giac", "giacomini", "giac@gmail.com", "5678");
//					
	//	imp.add(ut,1L);
					
					
			
		}
		
		
	

}

