package it.damianocantali.rest.crm.login;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
public class LoginRequest {	
	private String userName;	
	private String password;

}
