package it.damianocantali.rest.crm.serviceImpl;

import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import it.damianocantali.rest.crm.models.RoleType;
import it.damianocantali.rest.crm.models.Utente;
import it.damianocantali.rest.crm.models.pojo.UtentePojo;
import it.damianocantali.rest.crm.repositories.RoleRepository;
import it.damianocantali.rest.crm.repositories.UtenteRepository;
import it.damianocantali.rest.crm.serviceImpl.exception.AppServiceException;
import it.damianocantali.rest.crm.services.UtenteService;

@Service
public class UtenteImp implements UtenteService {
	@Autowired
	UtenteRepository utenteRepository;

	@Autowired
	RoleRepository repository;

	@Autowired
	PasswordEncoder encoder;

	@Autowired
	RoleRepository roleRepository;

	@Override
	@Transactional
	public Utente add(UtentePojo ut, long id) {
		try {
			Utente utente = new Utente();
			utente.setUserName(ut.getUserName());
			utente.setCognome(ut.getCognome());
			utente.setNome(ut.getNome());
			utente.setEmail(ut.getEmail());
			String hashedPassword = encoder.encode(ut.getPassword());
			utente.setPassword(hashedPassword);
			utenteRepository.save(utente);
			utente.getRole().add(repository.findById(id).get());
			return utenteRepository.save(utente);

		} catch (Exception e) {
			throw new AppServiceException(e);
		}

	}

	@Override
	@Transactional
	public Utente delete(long utente) {

		try {
			Utente u = utenteRepository.findById(utente).get();
			if (!u.getRole().equals(RoleType.ROLE_ADMIN)) {
				u.getRole().clear();
				utenteRepository.delete(u);
			}
			return u;
		} catch (Exception e) {
			throw new AppServiceException(e);
		}

	}

	@Override
	public Page<Utente> getAllUser(Pageable pageable) {
		try {
			return utenteRepository.findAll(pageable);

		} catch (Exception e) {
			throw new AppServiceException(e);
		}

	}

	@Override
	public Optional<Utente> getById(int id) {
		try {
			return utenteRepository.findById(id);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public Page<Utente> getByNome(String nome, Pageable pageable) {
		try {
			return utenteRepository.findByNome(nome, pageable);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public Utente update(Utente utente) {
		try {
			Optional<Utente> opt = utenteRepository.findById((int) utente.getId());
			if (opt.isPresent()) {
				Utente updatable = opt.get();
				if (utente.getEmail() != null)
					updatable.setEmail(utente.getEmail());
				if (utente.getNome() != null)
					updatable.setNome(utente.getNome());
				if (utente.getCognome() != null)
					updatable.setCognome(utente.getCognome());
				if (utente.getPassword() != null)
					updatable.setPassword(utente.getPassword());
				if (utente.getUserName() != null)
					updatable.setUserName(utente.getUserName());

				return utenteRepository.save(updatable);
			}

		} catch (Exception e) {

		}
		return utente;

	}

	@Override
	public Optional<Utente> getByEmail(String email) {
		try {
			return utenteRepository.findByEmail(email);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}

	}

	@Override
	public Optional<Utente> getByUserName(String userName) {
		try {
			return utenteRepository.findByUserName(userName);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}
		/**
		 * metodo che cripta le password di tutti gli utenti gia presenti 
		 * in database e non criptati in fase di salvataggio
		 */
	public void criptAll() {
		List<Utente> list = utenteRepository.findAll();
		for (Utente u : list) {
			String hashedPassword = encoder.encode(u.getPassword());
			u.setPassword(hashedPassword);
			utenteRepository.save(u);
		}

	}
}
