package it.damianocantali.rest.crm.models.pojo;

import java.util.Date;
import it.damianocantali.rest.crm.models.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AziendaPojo extends BaseEntity{
	private long id;
	private String nome;
	private String ragioneSociale;
	private long partitaIva;
	private String email;
	private Date dataUltimoContatto;
	private String pec;
	private String telefono;
	private long contatto;
	private long tipo;
	private long indirizzo;
	private long indirizzoOp;
	
}
