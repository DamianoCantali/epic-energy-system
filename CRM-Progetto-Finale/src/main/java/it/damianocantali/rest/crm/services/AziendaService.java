package it.damianocantali.rest.crm.services;

import java.math.BigDecimal;
import java.util.Date;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import it.damianocantali.rest.crm.models.Azienda;

public interface AziendaService {
	public Page<Azienda> findAllByOrderByNome(Pageable pageable);
	public Page<Azienda> findAllByOrderByCreatedAt(Pageable pageable);
	public Page<Azienda> findAllByOrderByDataUltimoContatto(Pageable pageable);
	public BigDecimal getAziendaByFatturatoAnnuale(int anno ,long id);
	public Page<Azienda> findByCreatedAt(Date createdAt, Date createdAt2,Pageable pageable);
	public Page<Azienda> findByDataUltimoContatto(Date dataUltimoContatto,Date dataUltimoContatto2,Pageable pageable);
	public Page<Azienda> findByNomeContains(String nome,Pageable pageable);
	public Page<Azienda> findAllByOrderByAcronym(Pageable pageable);
	
	public Azienda save(Azienda azienda);
	public void update(Azienda azienda);
	public String delete(long id);
}
