package it.damianocantali.rest.crm.serviceImpl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import it.damianocantali.rest.crm.models.StatoFattura;
import it.damianocantali.rest.crm.repositories.StatoFatturaRepository;
import it.damianocantali.rest.crm.services.StatoService;

@Service
public class StatoServiceImp implements StatoService {
	@Autowired
	StatoFatturaRepository fatturaRepository;

	@Override
	public Optional<StatoFattura> getById(long id) {
		try {
			return fatturaRepository.findById(id);
		} catch (Exception e) {
			throw new RuntimeException(e);

		}
	}

	@Override
	public StatoFattura save(StatoFattura statoFattura) {
		try {
			return fatturaRepository.save(statoFattura);
		} catch (Exception e) {
			throw new RuntimeException(e);

		}
	}

	@Override
	public StatoFattura update(StatoFattura statoFattura) {
		try {
			StatoFattura a = fatturaRepository.findById(statoFattura.getId()).get();
			if (statoFattura.getId() != 0L) {

				a.setStato(statoFattura.getStato());

			}
			return fatturaRepository.save(a);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}

	@Override
	public Page<StatoFattura> findByStato(String stato, Pageable pageable) {
		try {
			return fatturaRepository.findByStato(stato, pageable);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}

	@Override
	public StatoFattura findBySingleStato(String stato) {
		try {
			return fatturaRepository.findByStato(stato);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}
}
