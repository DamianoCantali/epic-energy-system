package it.damianocantali.rest.crm.models;


import javax.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
@EqualsAndHashCode(callSuper = true)

public class Contatto extends BaseEntity{
	private String email;
	private String nome;
	private String cognome;
	private long telefono;
	
}
