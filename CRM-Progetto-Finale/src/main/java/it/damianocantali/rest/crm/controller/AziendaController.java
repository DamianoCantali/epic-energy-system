package it.damianocantali.rest.crm.controller;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import it.damianocantali.rest.crm.autorizzazioni.JwtUtils;
import it.damianocantali.rest.crm.models.Azienda;
import it.damianocantali.rest.crm.models.pojo.AziendaPojo;
import it.damianocantali.rest.crm.serviceImpl.AziendaServiceImp;
import it.damianocantali.rest.crm.serviceImpl.ContattoServiceImpl;
import it.damianocantali.rest.crm.serviceImpl.IndirizzoServiceImp;
import it.damianocantali.rest.crm.serviceImpl.TipoAziendaImp;
import it.damianocantali.rest.crm.serviceImpl.UtenteImp;

@RestController
@RequestMapping("/azienda")
public class AziendaController {
	@Autowired
	AziendaServiceImp aziendaServiceImp;
	@Autowired
	ContattoServiceImpl contattoServiceImpl;
	@Autowired
	TipoAziendaImp tipoAziendaImp;
	@Autowired
	IndirizzoServiceImp indirizzoServiceImp;
	@Autowired
	JwtUtils jwtUtils;
	@Autowired
	UtenteImp utenteImp;

	/**
	 * @return tutte le aziende paginate in ordine di nome
	 */
	@GetMapping("/aziendepernome")
	@PreAuthorize("hasRole('ROLE_ADMIN')or hasRole('ROLE_USER')")
	public Page<Azienda> findAllByOrderByNome(@RequestParam(defaultValue = "0") Integer page,
			@RequestParam(defaultValue = "2") Integer size, @RequestParam(defaultValue = "asc") String dir,
			@RequestParam(defaultValue = "id") String sort, HttpServletRequest request) {

		Pageable pageable = PageRequest.of(page, size, Sort.Direction.fromString(dir), sort);
		Page<Azienda> pag = aziendaServiceImp.findAllByOrderByNome(pageable);
		return pag;

	}
	/**
	 * @return  tutte le aziende paginate in ordine di data creazione
	 */
	@GetMapping("/aziendeperdatacreazione")
	@PreAuthorize("hasRole('ROLE_ADMIN')or hasRole('ROLE_USER')")
	public Page<Azienda> findAllByOrderByCreatedAt(@RequestParam(defaultValue = "0") Integer page,
			@RequestParam(defaultValue = "2") Integer size, @RequestParam(defaultValue = "asc") String dir,
			@RequestParam(defaultValue = "id") String sort, HttpServletRequest request) {

		Pageable pageable = PageRequest.of(page, size, Sort.Direction.fromString(dir), sort);
		Page<Azienda> pag = aziendaServiceImp.findAllByOrderByCreatedAt(pageable);
		return pag;
	}
	/**
	 * @return  tutte le aziende paginate in ordine di data di ultimo contatto
	 */ 
	@GetMapping("/aziendeperdataultimocontatto")
	@PreAuthorize("hasRole('ROLE_ADMIN')or hasRole('ROLE_USER')")
	public Page<Azienda> findAllByOrderByDataUltimoContatto(@RequestParam(defaultValue = "0") Integer page,
			@RequestParam(defaultValue = "2") Integer size, @RequestParam(defaultValue = "asc") String dir,
			@RequestParam(defaultValue = "id") String sort, HttpServletRequest request) {

		Pageable pageable = PageRequest.of(page, size, Sort.Direction.fromString(dir), sort);
		Page<Azienda> pag = aziendaServiceImp.findAllByOrderByDataUltimoContatto(pageable);
		return pag;
	}
	/**
	 * @return  il fatturato annuo di un determinato anno tramite id Azienda
	 */
	@PostMapping("/aziendaperfatturato/{anno}/{id}")
	@PreAuthorize("hasRole('ROLE_ADMIN')or hasRole('ROLE_USER')")
	public BigDecimal findByFatturatoAnnuale(@PathVariable int anno, @PathVariable int id) {

		BigDecimal pag = aziendaServiceImp.getAziendaByFatturatoAnnuale(anno, id);
		return pag;

	}
	/**
	 * @return  tutte le aziende paginate e ordinate per data di creazione tramite un confronto tra date
	 */
	@PostMapping("/aziendapercreazione/{createdAt}/{createdAt2}")
	@PreAuthorize("hasRole('ROLE_ADMIN')or hasRole('ROLE_USER')")
	public Page<Azienda> findByCreatedAt(@PathVariable String createdAt, @PathVariable String createdAt2,
			@RequestParam(defaultValue = "0") Integer page, @RequestParam(defaultValue = "2") Integer size,
			@RequestParam(defaultValue = "asc") String dir, @RequestParam(defaultValue = "id") String sort,
			HttpServletRequest request) throws ParseException {
		Date st = new SimpleDateFormat("yyyy-MM-dd").parse(createdAt);
		Date en = new SimpleDateFormat("yyyy-MM-dd").parse(createdAt2);

		Pageable pageable = PageRequest.of(page, size, Sort.Direction.fromString(dir), sort);
		Page<Azienda> pag = aziendaServiceImp.findByCreatedAt(st, en, pageable);
		return pag;

	}
	/**
	 * @return  tutte le aziende paginate in ordine di data di ultimo contatto
	 */
	@PostMapping("/aziendaperdataultimocontatto")
	@PreAuthorize("hasRole('ROLE_ADMIN')or hasRole('ROLE_USER')")
	public Page<Azienda> findByDataUltimoContatto(@RequestBody Date dataUltimoContatto, Date dataUltimoContatto2,
			@RequestParam(defaultValue = "0") Integer page, @RequestParam(defaultValue = "2") Integer size,
			@RequestParam(defaultValue = "asc") String dir, @RequestParam(defaultValue = "id") String sort,
			HttpServletRequest request) {

		Pageable pageable = PageRequest.of(page, size, Sort.Direction.fromString(dir), sort);
		Page<Azienda> pag = aziendaServiceImp.findByDataUltimoContatto(dataUltimoContatto, dataUltimoContatto2,
				pageable);
		return pag;

	}
	/**
	 * @return  tutte le aziende paginate tramite parte del nome
	 */
	@PostMapping("/aziendepernomecontains")
	@PreAuthorize("hasRole('ROLE_ADMIN')or hasRole('ROLE_USER')")
	public Page<Azienda> findByNomeContains(@RequestParam String nome, @RequestParam(defaultValue = "0") Integer page,
			@RequestParam(defaultValue = "2") Integer size, @RequestParam(defaultValue = "asc") String dir,
			@RequestParam(defaultValue = "id") String sort, HttpServletRequest request) {

		Pageable pageable = PageRequest.of(page, size, Sort.Direction.fromString(dir), sort);
		Page<Azienda> pag = aziendaServiceImp.findByNomeContains(nome, pageable);
		return pag;

	}
	/**
	 * @return  tutte le aziende paginate e ordinate, tramite l'acronimo della provincia
	 */
	@GetMapping("/aziendeperacronym")
	@PreAuthorize("hasRole('ROLE_ADMIN')or hasRole('ROLE_USER')")
	public Page<Azienda> findAllByOrderByAcronym(@RequestParam(defaultValue = "0") Integer page,
			@RequestParam(defaultValue = "2") Integer size, @RequestParam(defaultValue = "asc") String dir,
			@RequestParam(defaultValue = "id") String sort, HttpServletRequest request) {

		Pageable pageable = PageRequest.of(page, size, Sort.Direction.fromString(dir), sort);
		return aziendaServiceImp.findAllByOrderByAcronym(pageable);

	}
	/**
	 * @return  salvataggio di un azienda
	 */
	@PostMapping("/salvaazienda")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@Transactional
	public String saveAzienda(@RequestBody AziendaPojo aziendaPojo) {
		Azienda a = Azienda.builder().nome(aziendaPojo.getNome()).ragioneSociale(aziendaPojo.getRagioneSociale())
				.partitaIva(aziendaPojo.getPartitaIva()).email(aziendaPojo.getEmail())
				.dataUltimoContatto(aziendaPojo.getDataUltimoContatto()).pec(aziendaPojo.getPec())
				.telefono(aziendaPojo.getTelefono())
				.contatto(contattoServiceImpl.getById(aziendaPojo.getContatto()).get())
				.tipoAzienda(tipoAziendaImp.getById(aziendaPojo.getTipo()).get())
				.indirizzoLegale(indirizzoServiceImp.getById(aziendaPojo.getIndirizzo()).get())
				.indirizzoOperativo(indirizzoServiceImp.getById(aziendaPojo.getIndirizzoOp()).get()).build();

		aziendaServiceImp.save(a);

		return "Azienda salvata con successo";
	}
	/**
	 * @return  modifica di un azienda
	 */
	@PostMapping("/modificaazienda")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@Transactional
	public String modificaAzienda(@RequestBody AziendaPojo aziendaPojo) {

		Azienda a = Azienda.builder().nome(aziendaPojo.getNome()).ragioneSociale(aziendaPojo.getRagioneSociale())
				.partitaIva(aziendaPojo.getPartitaIva()).email(aziendaPojo.getEmail())
				.dataUltimoContatto(aziendaPojo.getDataUltimoContatto()).pec(aziendaPojo.getPec())
				.telefono(aziendaPojo.getTelefono())
				.contatto(contattoServiceImpl.getById(aziendaPojo.getContatto()).get())
				.tipoAzienda(tipoAziendaImp.getById(aziendaPojo.getTipo()).get())
				.indirizzoLegale(indirizzoServiceImp.getById(aziendaPojo.getIndirizzo()).get())
				.indirizzoOperativo(indirizzoServiceImp.getById(aziendaPojo.getIndirizzoOp()).get()).build();
		a.setId(aziendaPojo.getId());

		aziendaServiceImp.update(a);

		return "Azienda modificata con successo";

	}
	/**
	 * @return  eliminazione di un azienda
	 */
	@DeleteMapping("/eliminaazienda/{id}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String elimina(@PathVariable long id) {
		return aziendaServiceImp.delete(id);

	}
}
