package it.damianocantali.rest.crm.services;

import java.util.Optional;

import it.damianocantali.rest.crm.models.TipoAzienda;

public interface TipoAziendaService {
	public TipoAzienda save(TipoAzienda tipoAzienda);
	public Optional<TipoAzienda> getById(Long id);
	public String delete(TipoAzienda tipoAzienda);
	
}
