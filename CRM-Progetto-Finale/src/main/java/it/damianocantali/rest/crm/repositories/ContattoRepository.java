package it.damianocantali.rest.crm.repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import it.damianocantali.rest.crm.models.Contatto;

public interface ContattoRepository extends JpaRepository<Contatto, Long>{


}
