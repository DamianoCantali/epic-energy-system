package it.damianocantali.rest.crm.serviceImpl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import it.damianocantali.rest.crm.models.Azienda;
import it.damianocantali.rest.crm.models.Fattura;
import it.damianocantali.rest.crm.models.StatoFattura;
import it.damianocantali.rest.crm.repositories.AziendaRepository;
import it.damianocantali.rest.crm.repositories.FatturaRepository;
import it.damianocantali.rest.crm.repositories.StatoFatturaRepository;
import it.damianocantali.rest.crm.services.FatturaService;

@Service
public class FatturaServiceImp implements FatturaService {
	@Autowired
	FatturaRepository fatturaRepository;
	@Autowired
	StatoFatturaRepository statoFatturaRepository;
	@Autowired
	AziendaRepository aziendaRepository;

	@Override
	public List<Fattura> getByAzienda(long id) {
		try {
			Azienda a = aziendaRepository.getById(id).get();
			return fatturaRepository.findByAzienda(a);
		} catch (Exception e) {
			throw new RuntimeException(e);

		}
	}

	@Override
	public Page<Fattura> getByStatoFattura(StatoFattura statoFattura, Pageable pageable) {
		try {
			return fatturaRepository.findByStatoFattura(statoFattura, pageable);
		} catch (Exception e) {
			throw new RuntimeException(e);

		}
	}

	@Override
	public Page<Fattura> getByData(Date data, Date data2, Pageable pageable) {
		try {
			return fatturaRepository.findByDataBetween(data, data2, pageable);
		} catch (Exception e) {
			throw new RuntimeException(e);

		}
	}

	@Override
	public Page<Fattura> getByDataYear(int anno, Pageable pageable) {
		try {
			return fatturaRepository.findByDataYear(anno, pageable);
		} catch (Exception e) {
			throw new RuntimeException(e);

		}
	}

	@Override
	public Page<Fattura> getByImportoBetween(BigDecimal primoImporto, BigDecimal secondoImporto, Pageable pageable) {
		try {
			return fatturaRepository.findByImportoBetween(primoImporto, secondoImporto, pageable);
		} catch (Exception e) {
			throw new RuntimeException(e);

		}
	}

	@Override
	public Optional<Fattura> getById(long id) {
		try {
			return fatturaRepository.findById(id);
		} catch (Exception e) {
			throw new RuntimeException(e);

		}
	}

	@Override
	@Transactional
	public Fattura save(Fattura fattura) {
		try {
			return fatturaRepository.save(fattura);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Fattura update(Fattura fattura) {
		try {
			Optional <Fattura> f = fatturaRepository.findById(fattura.getId());
			if(f.isPresent()) {
				f.get().setData(fattura.getData());
				f.get().setImporto(fattura.getImporto());
				f.get().setNumero(fattura.getNumero());
				f.get().setStatoFattura(fattura.getStatoFattura());
				
			}
			return fatturaRepository.save(f.get());
		} catch (Exception e) {
			throw new RuntimeException(e);

		}
	}

	@Override
	public String delete(long id) {
		try {
			Fattura f = fatturaRepository.findById(id).get();	
			fatturaRepository.delete(f);
			return "la fattura è stata eliminata";
		} catch (Exception e) {
			throw new RuntimeException(e);

		}
	}
}
