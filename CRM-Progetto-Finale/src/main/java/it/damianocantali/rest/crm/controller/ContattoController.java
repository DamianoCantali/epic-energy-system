package it.damianocantali.rest.crm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import it.damianocantali.rest.crm.models.Contatto;
import it.damianocantali.rest.crm.serviceImpl.ContattoServiceImpl;

@RestController
@RequestMapping("/contatto")
public class ContattoController {

	@Autowired
	ContattoServiceImpl contattoServiceImpl;
	
	/**
	 * @return salvataggio di un contatto
	 */
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@PostMapping("/salvacontatto")
	public String saveContatto(@RequestBody Contatto contatto) {
		contattoServiceImpl.save(contatto);
		return "contatto salvato con successo";
	}
	/**
	 * @return aggiornamento di un contatto
	 */
	@PostMapping("/aggiornacontatto")
	@PreAuthorize("hasRole('ROLE_ADMIN')or hasRole('ROLE_USER')")
	public String updateContatto(@RequestBody Contatto contatto) {
		contattoServiceImpl.update(contatto);
		return "contatto salvato con successo";
	}
	/**
	 * @return eliminazione di un contatto
	 */
	@DeleteMapping("/eliminacontatto/{id}")
	@PreAuthorize("hasRole('ROLE_ADMIN')or hasRole('ROLE_USER')")
	public String eliminaContatto(@PathVariable long id) {
		return contattoServiceImpl.delete(id);

	}

}
