package it.damianocantali.rest.crm.services;

import java.util.Optional;

import it.damianocantali.rest.crm.models.Contatto;

public interface ContattoService {
		public Contatto save(Contatto contatto);
		public Contatto update(Contatto contatto );
		public String delete(long id);
		public Optional<Contatto>getById(long id);
}
