package it.damianocantali.rest.crm.serviceImpl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import it.damianocantali.rest.crm.models.Citta;
import it.damianocantali.rest.crm.models.Provincia;
import it.damianocantali.rest.crm.repositories.CittaRepository;
import it.damianocantali.rest.crm.repositories.ProvinciaRepository;
import it.damianocantali.rest.crm.services.CittaService;

@Service
public class CittaServiceImpl implements CittaService {
	@Autowired
	ProvinciaRepository provinciaRepository;

	@Autowired
	CittaRepository cittaRepository;

	@Override
	public Citta add(Citta citta) {
		try {
			Provincia prov;
			Optional<Provincia> p = provinciaRepository.findByAcronym(citta.getProvince().getAcronym());
			if (!p.isPresent())
				prov = provinciaRepository.save(citta.getProvince());
			else
				prov = p.get();
			citta.setProvince(prov);
			return cittaRepository.save(citta);

		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}

	@Override
	public Citta findByName(String name) {
		try {
			return cittaRepository.findByName(name);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}

	@Override
	public Citta findByNameAndProvince(String name, Provincia province) {
		try {
			return cittaRepository.findByNameAndProvince(name, province);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}

}
