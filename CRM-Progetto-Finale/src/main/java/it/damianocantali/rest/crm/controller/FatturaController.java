package it.damianocantali.rest.crm.controller;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import it.damianocantali.rest.crm.models.Fattura;
import it.damianocantali.rest.crm.models.StatoFattura;
import it.damianocantali.rest.crm.models.pojo.FatturaPojo;
import it.damianocantali.rest.crm.serviceImpl.AziendaServiceImp;
import it.damianocantali.rest.crm.serviceImpl.FatturaServiceImp;
import it.damianocantali.rest.crm.serviceImpl.StatoServiceImp;

@RestController
@RequestMapping("/fattura")
public class FatturaController {
	@Autowired
	FatturaServiceImp fatturaServiceImp;
	@Autowired
	AziendaServiceImp aziendaServiceImp;
	@Autowired
	StatoServiceImp statoServiceImp;
	
	/**
	 * @return tutte le fatture di una determinata azienda
	 */
	@GetMapping("/fattureperazienda")
	@PreAuthorize("hasRole('ROLE_ADMIN')or hasRole('ROLE_USER')")
	public List<Fattura> findByAzienda(@RequestParam long id) {

		return fatturaServiceImp.getByAzienda(id);
	}
	/**
	 * @return tutte le fatture paginate e ottenute tramite stato fattura
	 */
	@PostMapping("/fatturaperstato")
	@PreAuthorize("hasRole('ROLE_ADMIN')or hasRole('ROLE_USER')")
	public Page<Fattura> findByStatoFattura(@RequestParam String statoFattura,
			@RequestParam(defaultValue = "0") Integer page, @RequestParam(defaultValue = "2") Integer size,
			@RequestParam(defaultValue = "asc") String dir, @RequestParam(defaultValue = "id") String sort) {

		Pageable pageable1 = PageRequest.of(page, size, Sort.Direction.fromString(dir), sort);
		Page<StatoFattura> stato = statoServiceImp.findByStato(statoFattura, pageable1);
		if (stato.isEmpty()) {
			return null;

		}
		List<StatoFattura> list = stato.getContent();
		return fatturaServiceImp.getByStatoFattura(list.get(0), pageable1);
	}
	/**
	 * @return tutte  le fatture paginate e ottenute tramite un confronto di date
	 */
	@PostMapping("/perdata/{data}/{data2}")
	@PreAuthorize("hasRole('ROLE_ADMIN')or hasRole('ROLE_USER')")
	public Page<Fattura> findByDataBetween(@PathVariable String data, @PathVariable String data2,
			@RequestParam(defaultValue = "0") Integer page, @RequestParam(defaultValue = "2") Integer size,
			@RequestParam(defaultValue = "asc") String dir, @RequestParam(defaultValue = "id") String sort)
			throws ParseException {
		Date st = new SimpleDateFormat("yyyy-MM-dd").parse(data);
		Date en = new SimpleDateFormat("yyyy-MM-dd").parse(data2);
		Pageable paging = PageRequest.of(page, size, Sort.Direction.fromString(dir), sort);
		return fatturaServiceImp.getByData(st, en, paging);
	}
	/**
	 * @return tutte le fatture paginate per un dato anno 
	 */
	@PostMapping("/peranno/{anno}")
	@PreAuthorize("hasRole('ROLE_ADMIN')or hasRole('ROLE_USER')")
	public Page<Fattura> findByAnno(@PathVariable int anno, @RequestParam(defaultValue = "0") Integer page,
			@RequestParam(defaultValue = "2") Integer size, @RequestParam(defaultValue = "asc") String dir,
			@RequestParam(defaultValue = "id") String sort) {
		Pageable paging = PageRequest.of(page, size, Sort.Direction.fromString(dir), sort);
		return fatturaServiceImp.getByDataYear(anno, paging);
	}
	/**
	 * @return tutte le fatture paginate per un dato importo
	 */
	@PostMapping("/perimporto")
	@PreAuthorize("hasRole('ROLE_ADMIN')or hasRole('ROLE_USER')")
	public Page<Fattura> findByData(@RequestParam BigDecimal importo1, @RequestParam BigDecimal importo2,
			@RequestParam(defaultValue = "0") Integer page, @RequestParam(defaultValue = "2") Integer size,
			@RequestParam(defaultValue = "asc") String dir, @RequestParam(defaultValue = "id") String sort) {

		Pageable paging = PageRequest.of(page, size, Sort.Direction.fromString(dir), sort);
		return fatturaServiceImp.getByImportoBetween(importo1, importo2, paging);

	}
	/**
	 * @return salvataggo di una fattura
	 */
	@PostMapping("/salvafattura/{idAzienda}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")

	public String save(@PathVariable long idAzienda, @RequestBody FatturaPojo fatturaPojo) {
		if (aziendaServiceImp.getById(idAzienda).isEmpty()) {
			return "azienda non presente in dataBase";
		}
		Fattura fattura = Fattura.builder().data(fatturaPojo.getData()).importo(fatturaPojo.getImporto())
				.azienda(aziendaServiceImp.getById(idAzienda).get()).numero(fatturaPojo.getNumero())
				.statoFattura(statoServiceImp.findBySingleStato(fatturaPojo.getStatoFattura()))

				.build();
		fatturaServiceImp.save(fattura);
		return "fattura salvata con successo";
	}
	/**
	 * @return modifica di una fattura
	 */
	@PostMapping("/modificafattura/{idAzienda}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public Fattura update(@PathVariable long idAzienda, @RequestBody FatturaPojo fatturaPojo) {
		Fattura fattura = Fattura.builder().data(fatturaPojo.getData()).importo(fatturaPojo.getImporto())
				.azienda(aziendaServiceImp.getById(idAzienda).get()).numero(fatturaPojo.getNumero())
				.statoFattura(statoServiceImp.findBySingleStato(fatturaPojo.getStatoFattura())).build();
		fattura.setId(fatturaPojo.getId());
		return fatturaServiceImp.update(fattura);
	}
	/**
	 * @return eliminazione di una fattura
	 */
	@PostMapping("/eliminafattura/{id}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String delete(@PathVariable long id) {
		return fatturaServiceImp.delete(id);
	}

}
