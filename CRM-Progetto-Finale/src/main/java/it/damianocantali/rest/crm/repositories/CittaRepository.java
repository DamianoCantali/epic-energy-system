package it.damianocantali.rest.crm.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;


import it.damianocantali.rest.crm.models.Citta;
import it.damianocantali.rest.crm.models.Provincia;

public interface CittaRepository extends JpaRepository<Citta, Long>{
	// TROVA TUTTE LE CITTA PER IL LORO ACRONIMO
	Page<Citta> findAllByProvinceAcronym(String acronym, Pageable pageable);
	// TROVA LA CITTA PER IL SUO NOME
	public Citta findByName(String name);
	// TROVA LA CITTA PER NOME E PROVINCIA
	public Citta findByNameAndProvince(String name,Provincia province);
}
