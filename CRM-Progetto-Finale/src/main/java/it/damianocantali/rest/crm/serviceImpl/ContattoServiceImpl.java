package it.damianocantali.rest.crm.serviceImpl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.damianocantali.rest.crm.models.Contatto;
import it.damianocantali.rest.crm.repositories.ContattoRepository;
import it.damianocantali.rest.crm.services.ContattoService;

@Service
public class ContattoServiceImpl implements ContattoService {
	@Autowired
	ContattoRepository contattoRepository;

	@Override
	public Contatto save(Contatto contatto) {
		try {
			return contattoRepository.save(contatto);
		} catch (Exception e) {
			throw new RuntimeException(e);
			
		}
	}

	@Override
	public Contatto update(Contatto contatto) {
		try {
			Contatto c = contattoRepository.findById(contatto.getId()).get();
			c.setCognome(contatto.getCognome());
			c.setEmail(contatto.getEmail()); 
			c.setNome(contatto.getNome());
			c.setTelefono(contatto.getTelefono());
			return save(c);
		} catch (Exception e) {
			throw new RuntimeException(e);
			
		}
	}

	@Override
	public String delete(long id) {
		try {
			contattoRepository.delete(contattoRepository.findById(id).get());
			 return"contatto eliminato con successo";
		} catch (Exception e) {
			throw new RuntimeException(e);
			
		}
	}

	@Override
	public Optional<Contatto> getById(long id) {
		try {
			return contattoRepository.findById(id);
		} catch (Exception e) {
			throw new RuntimeException(e);
			
		}
	}

	

}
