package it.damianocantali.rest.crm.models;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.JoinColumn;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import it.damianocantali.rest.crm.autorizzazioni.StringAttributeConverter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Utente extends BaseEntity{
	@Convert(converter = StringAttributeConverter.class)
	@Column(nullable = false, length = 125)
	private String nome;
	@Column(nullable = false, length = 125)
	private String userName;
	@Convert(converter = StringAttributeConverter.class)
	@Column(nullable = false, length = 125)
	private String cognome;
	@Column(nullable = false, length = 125,unique=true)
	@Convert(converter = StringAttributeConverter.class)
	private String email;
	@Column(nullable = false, length = 125)
	private String password;
	@Column(nullable = false)
	@ManyToMany(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinTable(
	            name="utente_role",
	            joinColumns= @JoinColumn(name="user_id", referencedColumnName="id"),
	            inverseJoinColumns= @JoinColumn(name="role_id", referencedColumnName="id")
	        )
		private Set<Role> role = new HashSet<>();
	//private Role ruolo;
}
