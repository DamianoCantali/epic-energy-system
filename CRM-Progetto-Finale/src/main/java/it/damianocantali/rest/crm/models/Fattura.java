package it.damianocantali.rest.crm.models;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
@EqualsAndHashCode(callSuper = true)
public class Fattura extends BaseEntity{
	private Date data;
	private BigDecimal importo;
	private int numero;
	@ManyToOne (fetch = FetchType.EAGER)
	@JsonIgnore
	private Azienda azienda;
	@ManyToOne (cascade = CascadeType.ALL)
	private StatoFattura statoFattura;
}
