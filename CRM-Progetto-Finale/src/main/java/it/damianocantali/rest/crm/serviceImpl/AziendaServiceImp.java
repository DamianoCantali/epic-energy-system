package it.damianocantali.rest.crm.serviceImpl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import javax.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import it.damianocantali.rest.crm.models.Azienda;
import it.damianocantali.rest.crm.models.Fattura;
import it.damianocantali.rest.crm.repositories.AziendaRepository;
import it.damianocantali.rest.crm.repositories.ContattoRepository;
import it.damianocantali.rest.crm.repositories.FatturaRepository;
import it.damianocantali.rest.crm.repositories.IndirizzoRepository;
import it.damianocantali.rest.crm.services.AziendaService;

@Service
public class AziendaServiceImp implements AziendaService {
@Autowired
AziendaRepository aziendaRepository;
@Autowired
FatturaRepository  fatturaRepository;
@Autowired
IndirizzoRepository indirizzoRepository;
@Autowired
ContattoRepository contattoRepository;

	@Override
	public Page<Azienda> findAllByOrderByNome(Pageable pageable) {
		try {
			return aziendaRepository.findAllByOrderByNome(pageable);
		} catch (Exception e) {
			throw new RuntimeException(e);	
		}  	
	}
	

	@Override
	public Page<Azienda> findAllByOrderByCreatedAt(Pageable pageable) {
		try {
			return aziendaRepository.findAllByOrderByCreatedAt(pageable);
		} catch (Exception e) {
			throw new RuntimeException(e);	
		}
	}

	@Override
	public Page<Azienda> findAllByOrderByDataUltimoContatto(Pageable pageable) {
		try {
			return aziendaRepository.findAllByOrderByDataUltimoContatto(pageable);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public BigDecimal getAziendaByFatturatoAnnuale(int anno , long id) {
		try {
			return fatturaRepository.getFatturatoAnnualeFromAzienda( anno ,id);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Page<Azienda> findByCreatedAt(Date createdAt, Date createdAt2,Pageable pageable) {
		try {
			return aziendaRepository.findByCreatedAtBetween(createdAt,createdAt2, pageable);
		} catch (Exception e) {
			throw new RuntimeException(e);
			
		}
	}

	@Override
	public Page<Azienda> findByDataUltimoContatto(Date dataUltimoContatto, Date dataUltimoContatto2, Pageable pageable) {
		try {
			return aziendaRepository.findByDataUltimoContattoBetween(dataUltimoContatto, dataUltimoContatto2,pageable);
		} catch (Exception e) {
			throw new RuntimeException(e);
			
		}
	}

	@Override
	public Page<Azienda> findByNomeContains(String nome, Pageable pageable) {
		try {
			return aziendaRepository.findByNomeContains(nome, pageable);
			
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
	}

	@Override
	public Azienda save(Azienda azienda) {
		try {
			
			return aziendaRepository.save(azienda);
		} catch (Exception e) {
			throw new RuntimeException(e);
			
		}
	}

	@Override
	public void update(Azienda azienda) {
		
			Optional <Azienda> a = aziendaRepository.findById(azienda.getId());
			if(a.isPresent()) {
				
				a.get().setNome(azienda.getNome());
				a.get().setRagioneSociale(azienda.getRagioneSociale());
				a.get().setPartitaIva(azienda.getPartitaIva());
				a.get().setEmail(azienda.getEmail());
				a.get().setDataUltimoContatto(azienda.getDataUltimoContatto());
				a.get().setPec(azienda.getPec());
				a.get().setContatto(azienda.getContatto());
				a.get().setTipoAzienda(azienda.getTipoAzienda());
				a.get().setIndirizzoLegale(azienda.getIndirizzoLegale());
				a.get().setIndirizzoOperativo(azienda.getIndirizzoOperativo());
				
				
				aziendaRepository.save(a.get());
			}else {
				throw new EntityNotFoundException("l'azienda con questo id " + azienda.getId() +" non è stata");
			}
			
		}
	

	@Override
	public String delete(long id) {
		try {
			Azienda a = aziendaRepository.findById(id).get();  
			
		
	List<Fattura>list=	fatturaRepository.findByAzienda(a );
	
		
		for(Fattura f : list ) {
			fatturaRepository.delete(f);
		}
		
			 return"azienda eliminata con successo";
		} catch (Exception e) {
			throw new RuntimeException(e);
			
		}
	}

	@Override
	public Page<Azienda> findAllByOrderByAcronym(Pageable pageable) {
		try {
			return aziendaRepository.findAllByOrderByAcronym(pageable) ;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
	}
	public Optional< Azienda> getById( long id) {
		return aziendaRepository.getById(id);
	}
	}


