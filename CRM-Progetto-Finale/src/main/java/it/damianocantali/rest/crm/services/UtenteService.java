package it.damianocantali.rest.crm.services;


import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import it.damianocantali.rest.crm.models.Utente;
import it.damianocantali.rest.crm.models.pojo.UtentePojo;



public interface UtenteService {
	Utente add(UtentePojo utente,long id);
	Utente delete(long utente);
	Utente update(Utente utente);
	Page<Utente> getAllUser( Pageable  pageable);
	Optional<Utente>getById(int id);
	Page<Utente>getByNome(String nome, Pageable  pageable);
	Optional<Utente>getByEmail(String email );
	Optional<Utente>getByUserName(String userName );
}	
