package it.damianocantali.rest.crm.repositories;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Optional;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import it.damianocantali.rest.crm.models.Azienda;

public interface AziendaRepository extends JpaRepository<Azienda, Long>{
	
	// TROVA TUTTE LE AZIENDE PER NOME
	public Page<Azienda> findAllByOrderByNome(Pageable pageable);
	// TROVA TUTTE LE AZIENDE PER DATA DI CREAZIONE
	public Page<Azienda> findAllByOrderByCreatedAt(Pageable pageable);
	// TROVA TUTTE LE AZIENDE PER DATA DI ULTIMO CONTATTO
	public Page<Azienda> findAllByOrderByDataUltimoContatto(Pageable pageable);
	// TROVA TUTTE LE AZIENDE PER PROVINCIA
	@Query("select a from Azienda as a order by indirizzoLegale.comune.province.acronym")
	public Page<Azienda> findAllByOrderByAcronym(Pageable pageable);
	// TROVA L'AZIENDA PER LA DATA DI CREAZIONE
	public Page<Azienda> findByCreatedAtBetween(Date createdAt ,Date createdAt2,Pageable pageable);
	// TROVA L'AZIENDA PER LA DATA DI ULTIMO CONTATTO
	public Page<Azienda> findByDataUltimoContattoBetween(Date dataUltimoContatto,Date dataUltimoContatto2,Pageable pageable);
	// TROVA L'AZIENDA PER PARTE DEL NOME
	public Page<Azienda> findByNomeContains(String nome,Pageable pageable);
	// TROVA L'AZIENDA PER IMPORTO FATTURATO ANNUALE
	@Query("select SUM(f.importo) from Fattura as f where YEAR(f.data) = :anno and f.azienda.id = :id")
	public BigDecimal getFatturatoAnnualeByAzienda(int anno, long id);
	// GET DELL'AZIENDA PER ID
	public Optional<Azienda>getById(long id);
}
