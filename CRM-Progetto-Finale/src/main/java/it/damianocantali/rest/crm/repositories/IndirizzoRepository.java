package it.damianocantali.rest.crm.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import it.damianocantali.rest.crm.models.Azienda;

import it.damianocantali.rest.crm.models.Indirizzo;

public interface IndirizzoRepository extends JpaRepository<Indirizzo, Long>{
	
}
