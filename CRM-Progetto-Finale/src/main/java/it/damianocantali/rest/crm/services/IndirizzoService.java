package it.damianocantali.rest.crm.services;

import java.util.Optional;

import it.damianocantali.rest.crm.models.Indirizzo;

public interface IndirizzoService {
	
		public Optional<Indirizzo> getById(long id);
		public Indirizzo update(Indirizzo indirizzoAgg, String nomecomune, String siglaprov);		
		Indirizzo save(Indirizzo indirizzo, String nomecomune, String siglaprov);
}
			
			
			
