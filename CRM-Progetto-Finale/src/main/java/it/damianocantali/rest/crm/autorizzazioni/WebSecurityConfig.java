package it.damianocantali.rest.crm.autorizzazioni;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import it.damianocantali.rest.crm.sicurezza.UtenteDetailsService;









@SuppressWarnings("deprecation")
@Configuration
@EnableWebSecurity

@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
	UtenteDetailsService utenteDetailsService;
	
	@Autowired
	private AuthEntryPointUnauthorizedJwt unauthorizedHandler;

	@Bean
	public AuthTokenFilter authenticationJwtTokenFilter() {
		return new AuthTokenFilter();
	}
		
	
	@Override
	public void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
		// Aggiunge l'autenticazione utilizzando userDetailsService passato come argomento e il tipo di encoder scelto
		authenticationManagerBuilder.userDetailsService(utenteDetailsService).passwordEncoder(this.passwordEncoder());
	}
	
	
	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

	
	@Bean
	public PasswordEncoder passwordEncoder() {
		
		return new BCryptPasswordEncoder();
		
	}
	
	
	// Il metodo configure(HttpSecurity) definisce i criteri di protezione HTTP
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		// Imposta il filtro CORS
		http.cors().and()
			// In questo esempio disabilita il filtro CSRF
			.csrf().disable()
			// Definisce la classe che gestisce gli accessi non autorizzati
			.exceptionHandling().authenticationEntryPoint(unauthorizedHandler).and()
			// Imposta la policy delle Session
			.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
			// Definisce quali percorsi URL sono autorizzati (in questo caso tutti i percorsi e relative directory)
			.authorizeRequests().antMatchers("/**").permitAll()
			// Ogni request deve essere autenticata
			.anyRequest().authenticated();
		// Aggiunge il filtro di autenticazione
		http.addFilterBefore(authenticationJwtTokenFilter(), UsernamePasswordAuthenticationFilter.class);
	}
}
