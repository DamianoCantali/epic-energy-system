package it.damianocantali.rest.crm.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import it.damianocantali.rest.crm.models.StatoFattura;

public interface StatoFatturaRepository extends JpaRepository<StatoFattura, Long>{
	// TROVA TUTTI GLI STATI DELL FATTURA TRAMITE LO STATO STESSO
	public Page<StatoFattura>findByStato(String stato,Pageable paegeble);
	public StatoFattura findByStato(String stato);

}
