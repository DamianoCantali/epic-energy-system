package it.damianocantali.rest.crm.models;


import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
@EqualsAndHashCode(callSuper = true)
public class Azienda extends BaseEntity{
	private String nome;
	private String ragioneSociale;
	private long partitaIva;
	private String email;

	private Date dataUltimoContatto;
	private String pec;
	private String telefono;
	@ManyToOne 
	private Contatto contatto;
	@ManyToOne 
	private TipoAzienda tipoAzienda;
	@OneToOne (cascade = CascadeType.ALL)
	private Indirizzo indirizzoLegale;
	@OneToOne 
	(cascade = CascadeType.ALL)
	private Indirizzo indirizzoOperativo;
}
