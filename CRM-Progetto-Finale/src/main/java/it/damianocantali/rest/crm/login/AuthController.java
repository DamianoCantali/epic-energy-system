package it.damianocantali.rest.crm.login;


import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import it.damianocantali.rest.crm.autorizzazioni.JwtUtils;
import it.damianocantali.rest.crm.repositories.UtenteRepository;
import it.damianocantali.rest.crm.sicurezza.UtenteDetailsImp;












@RestController
@RequestMapping("/api")
public class AuthController {
	
	@Autowired
	AuthenticationManager authenticationManager;
	
	@Autowired
	UtenteRepository userRepository;
	
	@Autowired
	JwtUtils jwtUtils;
	

	
	@PostMapping("/login")
	public ResponseEntity<?> authenticateUser(@RequestParam String userName, @RequestParam String password, HttpServletResponse response) {
		LoginRequest loginRequest = new LoginRequest(userName, password);
		// Usa l'AuthenticationManager per autenticare i parametri della request
		Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginRequest.getUserName(), loginRequest.getPassword()));
		// Ottiene i privilegi dell'utente
		authentication.getAuthorities();
		
		// Ottiene il SecurityContext
		SecurityContextHolder.getContext().setAuthentication(authentication);
		// Genera il token
		String jwt = jwtUtils.generateJwtToken(authentication);
		
		// Ottiene i dati dell'utente 
		UtenteDetailsImp userDetails = (UtenteDetailsImp) authentication.getPrincipal();
		
		// Ottiene i ruoli dell'utente
		List<String> roles = userDetails.getAuthorities().stream().map(item -> item.getAuthority()).collect(Collectors.toList());
		
		// Restituisce la response con status 200, token e dati dell'utente
		return ResponseEntity.ok(new LoginResponse(jwt, userDetails.getId(), userDetails.getUsername(), roles, userDetails.getExpirationTime()));
	}
}

