package it.damianocantali.rest.crm.serviceImpl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.damianocantali.rest.crm.models.TipoAzienda;
import it.damianocantali.rest.crm.repositories.TipoAziendaRepository;
import it.damianocantali.rest.crm.services.TipoAziendaService;
@Service
public class TipoAziendaImp implements TipoAziendaService {
@Autowired
TipoAziendaRepository aziendaRepository;
	@Override
	public TipoAzienda save(TipoAzienda tipoAzienda) {
		try {
			return aziendaRepository.save(tipoAzienda);
		} catch (Exception e) {
			throw new RuntimeException(e);
			
		}
	}

	@Override
	public Optional<TipoAzienda> getById(Long id) {
		try {
			return aziendaRepository.findById(id);
		} catch (Exception e) {
			throw new RuntimeException(e);
			
		}
	}

	@Override
	public String delete(TipoAzienda tipoAzienda) {
		try {
			aziendaRepository.delete(tipoAzienda);
			 return "Il tipo azienda è stato eliminato";
		} catch (Exception e) {
			throw new RuntimeException(e);
			
		}
	}

}
