package it.damianocantali.rest.crm.sicurezza;

import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import it.damianocantali.rest.crm.models.Utente;
import it.damianocantali.rest.crm.repositories.UtenteRepository;

@Service
public class UtenteDetailsService implements UserDetailsService {

	@Autowired
	UtenteRepository utenteRepository;

	@Override
	// Il metodo viene eseguito in una transazione DB
	@Transactional
	// Cerca l'utente nel DB e ritorna l'utente tramite l'implementazione di
	// UserDetailsImpl o un'eccezione
	public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
		Optional<Utente> user = utenteRepository.findByUserName(userName);
		if (user.isPresent()) {
			return UtenteDetailsImp.build(user.get());
		} else {
			throw new UsernameNotFoundException("Utente " + userName + "non trovato!");
		}
	}

}
