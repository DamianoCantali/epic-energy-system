package it.damianocantali.rest.crm.models.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UtentePojo {
	
	private String nome;
	
	private String userName;
	private String cognome;
	private String email;
	private String password;
	//private List<String>ruoli;

}
