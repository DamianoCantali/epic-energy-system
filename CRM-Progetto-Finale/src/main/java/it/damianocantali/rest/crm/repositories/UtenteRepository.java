package it.damianocantali.rest.crm.repositories;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.damianocantali.rest.crm.models.Utente;




@Repository
public interface UtenteRepository  extends JpaRepository<Utente, Integer> {
	// TROVA TUTTI GLI UTENTI PER NOME
	Page<Utente>findByNome(String nome, Pageable pageble);
	// TROVA UN UTENTE PER EMAIL
	Optional<Utente>findByEmail(String email);
	// TROVA UN UTENTE PER USERNAME
	Optional<Utente>findByUserName(String userName);
	// TROVA UN UTENTE PER ID
	Optional<Utente>findById(long id);
	
	
	

}
