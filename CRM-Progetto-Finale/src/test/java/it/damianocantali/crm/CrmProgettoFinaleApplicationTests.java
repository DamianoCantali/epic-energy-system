package it.damianocantali.crm;

import java.math.BigDecimal;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.util.Date;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import it.damianocantali.rest.crm.CrmProgettoFinaleApplication;
import it.damianocantali.rest.crm.models.Azienda;
import it.damianocantali.rest.crm.models.pojo.DatePojo;
import it.damianocantali.rest.crm.serviceImpl.AziendaServiceImp;
import it.damianocantali.rest.crm.services.FatturaService;

@SpringBootTest(classes=CrmProgettoFinaleApplication.class)
class CrmProgettoFinaleApplicationTests {

	@Autowired
	AziendaServiceImp aziendaServiceImp;
	FatturaService fatturaServiceImp;
	Azienda azie;
	Pageable paging;
	DatePojo datapojo;
	
	
	@BeforeEach
public void create()  throws Exception  {

		paging = PageRequest.of(0,1 ,Sort.Direction.ASC,"id");
		datapojo = new DatePojo(new Date(7-18-2008),new Date(1-10-2022));
		azie = aziendaServiceImp.getById(4).get();
	}


	@Test
	public void testGetAllByNome() {
		assertThat(aziendaServiceImp.findAllByOrderByNome(paging).getSize()).isEqualTo(1);
	}
	@Test
	public void testGetAllByAcronym() {
		assertThat(aziendaServiceImp.findAllByOrderByAcronym(paging).getSize()).isEqualTo(1);
	}
	
	@Test
	public void testGetByDate() {
		assertThat(aziendaServiceImp.findByCreatedAt(datapojo.getData1(),datapojo.getData2(),paging).getSize()).isEqualTo(1);
	}
	@Test
	public void testGetByNomeContains() {
		assertThat(aziendaServiceImp.findByNomeContains("zucc",paging).getSize()).isEqualTo(1);
	}
	
}
