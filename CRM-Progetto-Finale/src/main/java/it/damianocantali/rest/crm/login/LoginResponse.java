package it.damianocantali.rest.crm.login;


import java.util.Date;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
public class LoginResponse {
	// Token
	private String token;	
	// Imposta il prefisso che indica il tipo di Token
	private final String type = "Bearer";
	// Dati dell'utente
	private int id;
	private String userName;
	private List<String> roles;
	private Date expirationTime;
}
